def entry_file(filename):
  file = open(filename, 'r')
  lines = file.readlines()
  file.close()
  program_deff=lines[0]
  program_deff= program_deff.replace('q','')

  index = program_deff.find('=')
  program_deff=program_deff[index+1:] #remove o nome do automato

  states_start = program_deff.find('{')
  states_end = program_deff.find('}')
  states = program_deff[ states_start+1 : states_end].split(',')
  program_deff = program_deff[states_end+2:]

  symbols_start = program_deff.find('{')
  symbols_end = program_deff.find('}')
  symbols = program_deff[ symbols_start+1 : symbols_end].split(',')
  program_deff = program_deff[symbols_end+2:]

  init_state_index =  program_deff.find(',') 
  init_state = program_deff[ :init_state_index]
  program_deff = program_deff[init_state_index:]

  end_states_start = program_deff.find('{')
  end_states_end = program_deff.find('}')
  end_states = program_deff[ end_states_start+1 : end_states_end].split(',')

  transitions = lines[2:]
  #.replace(['(',')'],' ')

  newfile = open('temp.txt','w')
  newfile.write(str(len(states))+'\n')
  newfile.write(''.join(symbols)+'\n')
  newfile.write(' '.join(end_states)+'\n')
  newfile.write(str(init_state)+'\n')
  for x in range(len(transitions)):
    transitions[x] = transitions[x].replace('q','')
    transitions[x] = transitions[x].replace('(','')
    transitions[x] = transitions[x].replace(',',' ')
    transitions[x] = transitions[x].replace(')',' ')
    transitions[x] = transitions[x].replace('=','')
    newfile.write(transitions[x])

  newfile.close()