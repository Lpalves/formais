import sys, os
from finite_automata import NFA, DFA
import handler

if sys.version_info >= (3, 0):
    filename = input("Enter the name of the NFA file: ")
    testWord = input("Enter the word you want to test: ")
elif sys.version_info >= (2, 0):
    filename = raw_input('Enter the name of the NFA file: ')
    testWord = raw_input('Enter the word you want to test: ')

handler.entry_file(filename)

file = open('temp.txt', 'r')
lines = file.readlines()
file.close()

nfa = NFA()
dfa = DFA()

nfa.construct_nfa_from_file(lines)
dfa.convert_nfa(nfa)
dfa.print_automata()
print( "Result: ",str(dfa.compute_word_recursive(testWord)))
os.remove('temp.txt')
