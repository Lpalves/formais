class NFA:
    def __init__(self):
        self.number_states = 0
        self.states = []
        self.symbols = []
        self.number_final_states = 0
        self.final_states = []
        self.start_state = 0
        self.transition_functions = []

    def init_states(self):
        self.states = list(range(self.number_states))

    def print_nfa(self):
        print(self.number_states)
        print(self.states)
        print(self.symbols)
        print(self.number_final_states)
        print(self.final_states)
        print(self.start_state)
        print(self.transition_functions)

    def construct_nfa_from_file(self, lines):
        self.number_states = int(lines[0])                     #get the aumont of states from line 1
        self.init_states()                                  #initialize de states array
        self.symbols = list(lines[1].strip())               #get the symbols from the line 2 of the file

        final_states_line = lines[2].split(" ")         #get acceptance states from line 3
        for index in range(len(final_states_line)):     #make an array with the acceptance states
            if index == 0:
                self.number_final_states = int(final_states_line[index])
            else:
                self.final_states.append(int(final_states_line[index]))
        
        self.startState = int(lines[3])                     #get start state from line 4
        
        
        for index in range(4, len(lines)):
            transition_func_line = lines[index].split(" ")
            
            starting_state = int(transition_func_line[0])
            transition_symbol = transition_func_line[1]
            ending_state = int(transition_func_line[2])
            
            transition_function = (starting_state, transition_symbol, ending_state);
            self.transition_functions.append(transition_function)        


class DFA:
    def __init__(self):
        self.number_states = 0
        self.symbols = []
        self.number_final_states = 0
        self.final_states = []
        self.start_state = 0
        self.transition_functions = []
        self.q = []
        self.transition_dictionary = {}
    
    def construct_transition_dictionary(self):
        for transition in self.transition_functions:
            state = str( transition[0])
            character = str( transition[1])
            destiny_state = str( transition[2])
            if state in self.transition_dictionary:
                self.transition_dictionary [state] [character]= destiny_state
            else:
                self.transition_dictionary [state]={ character : destiny_state}    

    def convert_nfa(self, nfa):
    #Convert the given NFA into a DFA
        
        self.symbols = nfa.symbols
        self.start_state = nfa.start_state

        nfa_transition_dict = {}
        dfa_transition_dict = {}
        
        # Combine NFA transitions
        for transition in nfa.transition_functions:
            starting_state = transition[0]
            transition_symbol = transition[1]
            ending_state = transition[2]
            
            if (starting_state, transition_symbol) in nfa_transition_dict:
                nfa_transition_dict[(starting_state, transition_symbol)].append(ending_state)
            else:
                nfa_transition_dict[(starting_state, transition_symbol)] = [ending_state]

        self.q.append((0,))
        
        # Convert NFA transitions to DFA transitions
        for dfa_state in self.q:
            for symbol in nfa.symbols:
                if len(dfa_state) == 1 and (dfa_state[0], symbol) in nfa_transition_dict:
                    dfa_transition_dict[(dfa_state, symbol)] = nfa_transition_dict[(dfa_state[0], symbol)]
                    
                    if tuple(dfa_transition_dict[(dfa_state, symbol)]) not in self.q:
                        self.q.append(tuple(dfa_transition_dict[(dfa_state, symbol)]))
                else:
                    destinations = []
                    final_destination = []
                    
                    for nfa_state in dfa_state:
                        if (nfa_state, symbol) in nfa_transition_dict and nfa_transition_dict[(nfa_state, symbol)] not in destinations:
                            destinations.append(nfa_transition_dict[(nfa_state, symbol)])
                    
                    if not destinations:
                        final_destination.append(None)
                    else:  
                        for destination in destinations:
                            for value in destination:
                                if value not in final_destination:
                                    final_destination.append(value)
                        
                    dfa_transition_dict[(dfa_state, symbol)] = final_destination
                        
                    if tuple(final_destination) not in self.q:
                        self.q.append(tuple(final_destination))

        # Convert NFA states to DFA states            
        for key in dfa_transition_dict:
            self.transition_functions.append((self.q.index(tuple(key[0])), key[1], self.q.index(tuple(dfa_transition_dict[key]))))
        
        for q_state in self.q:
            for nfa_accepting_state in nfa.final_states:
                if nfa_accepting_state in q_state:
                    self.final_states.append(self.q.index(q_state))
                    self.number_final_states += 1
        
        self.construct_transition_dictionary()

    def print_automata(self):
    #Print the definition of the automata on screen
        states = map(str,range(len(self.q)))
        end_states= map(str,self.final_states)
        states = ','.join(states)
        symbols = ','.join(self.symbols)
        end_states = ','.join(end_states)
        automata_deff='AUTOMATO=({'+states+'},{'+symbols+'},'+str(self.start_state)+',{'+end_states+'})'
        for x in range(len(self.q)):
            automata_deff = automata_deff.replace(str(x),'q'+str(x))

        print (automata_deff)
        print ("Prog")
        self.transition_functions = sorted(self.transition_functions)
        for x in range(len(sorted(self.transition_functions))):
            transition = list(self.transition_functions[x])
            print ("(q"+str(transition[0])+','+str(transition[1])+')=q'+str(transition[2]))

    def compute_word_recursive (self, word= False,state= False):
    #Make de computation of a given string, returns the state if final else returns False
        if not state:
            return self.compute_word_recursive( word, str(self.start_state))
        elif not word:
            if int(state) in self.final_states:
                return state
            else:
                return False
        elif state in self.transition_dictionary and word[0] in self.transition_dictionary[state]:
            return self.compute_word_recursive( word[1:], self.transition_dictionary[state][word[0]])
        else:
            return False 
